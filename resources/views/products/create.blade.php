@extends('template')

@section('content')

    <div class="col-md-12">



        <form action="/products" method="post" enctype="multipart/form-data" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            <div class="form-group">

                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control">

            </div>

            <div class="form-group">

                <label for="thumbnail"> Thumbnail </label>
                <input type="file" class="form-control" multiple name="thumbnail[]" id="thumbnail">

            </div>

            <div class="form-group">

                <label for="slug">Slug:</label>
                <input type="text" name="slug" id="slug" class="form-control">

            </div>

            <div class="form-group">

                <label for="price">Price:</label>
                <input type="text" name="price" id="price" class="form-control">

            </div>

            <div class="form-group">

                <label for="description">Description</label>
                <textarea name="description" id="description" class="form-control"></textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">Save</button>
            </div>

        </form>

    </div>

@endsection

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Create new product:</h1>
        </div>
    </div>
@endsection