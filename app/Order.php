<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_name','email','phone'];

    public function products(){

        return $this->belongsToMany(Product::class)->withPivot('amount');

    }
}
